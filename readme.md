### MLS 2017 AI 将棋の棋譜解析アプリ

requirements

* python 3.6.3
* anaconda(bokeh, numpy)
* 将棋ソフト「技巧」(https://github.com/gikou-official/Gikou)

動かし方

1. フォルダgikou2_winに、将棋ソフト「技巧」をダウンロード・解凍する
2. bokeh serveと入力し、Bokeh Serverを起動する
3. python bokeh_run.pyと入力し、plotを実行する（別のプロセス）

