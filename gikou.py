import os
import subprocess
import re
import numpy as np

gikou_path = r"gikou2_win\gikou.exe"
gikou_dir = os.path.dirname(gikou_path)


class Gikou:

    @classmethod
    def run(cls, moves, byoyomi):
        g = Gikou(moves, byoyomi)
        ret = g.run_process(moves, byoyomi)
        return ret

    def __init__(self, moves, byoyomi):
        self.p = None
        self.moves = moves.strip().split(" ")
        self.byoyomi = byoyomi

    def run_process(self, moves, byoyomi):

        self.p = subprocess.Popen((gikou_path,), stdin=subprocess.PIPE, stdout=subprocess.PIPE, cwd=gikou_dir)

        self.writeline("usi")
        self.wait_message("usiok")
        self.writeline("isready")
        self.wait_message("readyok")
        self.writeline("usinewgame")

        scores = []
        pvs = []
        for step in range(0, len(self.moves) + 1):
            st_pos = "position startpos moves {}".format(" ".join(self.moves[:step]))
            st_go = "go btime 0 wtime 0 byoyomi {}".format(self.byoyomi)
            score = None
            while score is None:
                self.writeline(st_pos)
                self.writeline(st_go)
                ret = self.wait_bestmove()
                score = self.get_score(ret, step)
                pv = self.get_pv(ret)
            scores.append(score)
            pvs.append(pv)

            print("{} score: {}, pv: {}".format(step, score, pv))

        return {"scores": scores, "pvs": pvs}

    def wait_message(self, message_to_wait):
        while True:
            line = self.p.stdout.readline()
            line = line.decode().strip()
            print(line)
            if (line.strip() == message_to_wait): return

    def wait_bestmove(self):
        lines = []
        for i in range(100):
            line = self.p.stdout.readline()
            line = line.decode().strip()
            lines.append(line)
            if line.strip().startswith("bestmove"): break
            if "No legal moves" in line:
                return ["score cp -9999", "pv "]
            # print(lines)
        return lines

    def get_score(self, lines, step):
        if len(lines) < 2: return None
        target = lines[-2]

        match_mate = re.match(r".*score\smate\s(-?\d*)", target)
        match = re.match(r".*score\scp\s(-?\d*)", target)

        if match_mate is not None:
            score = np.sign(int(match_mate[1])) * 9999
        elif match is not None:
            score = int(match[1])
        else:
            return None

        score = max(-2000, min(score, 2000))  # floor at 2000
        if step % 2 == 1:
            score *= -1

        return score

    def get_pv(self, lines):
        if len(lines) < 2: return None
        target = lines[-2]
        match = re.match(r".*pv\s(.*)", target)
        if match is None: return None
        return match[1]

    def writeline(self, message):
        self.p.stdin.write((message + "\r\n").encode("utf-8"))
        self.p.stdin.flush()
