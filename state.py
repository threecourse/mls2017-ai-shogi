from copy import deepcopy


class State:
    """SFENからboardを作成"""

    def __init__(self):
        self.board = None
        self.board_promoted = None
        self.moti = None

    def copy(self):
        st = State()
        st.board = deepcopy(self.board)
        st.board_promoted = deepcopy(self.board_promoted)
        st.moti = deepcopy(self.moti)
        return st

    def change_board(self, y, x, c):
        line = self.board[y]
        line = line[:x] + c + line[x + 1:]
        self.board[y] = line

    def change_board_promoted(self, y, x, c):
        line = self.board_promoted[y]
        line = line[:x] + c + line[x + 1:]
        self.board_promoted[y] = line

    @classmethod
    def koma(cls, piece, promoted):
        """sfenの駒と成り表記から駒の感じを返す"""

        if piece == " ": return (" ", True)
        key = (piece.lower(), promoted)

        dic = {("p", " "): "歩",
               ("p", "+"): "と",
               ("l", " "): "香",
               ("l", "+"): "杏",
               ("n", " "): "桂",
               ("n", "+"): "圭",
               ("s", " "): "銀",
               ("s", "+"): "全",
               ("g", " "): "金",
               ("b", " "): "角",
               ("b", "+"): "馬",
               ("r", " "): "飛",
               ("r", "+"): "龍",
               ("k", " "): "王", }
        return dic[key], piece.islower()

    @classmethod
    def start_state(cls):
        """初期盤面を返す"""

        s = State()
        s.board = ["lnsgkgsnl",
                   " r     b ",
                   "ppppppppp",
                   "         ",
                   "         ",
                   "         ",
                   "PPPPPPPPP",
                   " B     R ",
                   "LNSGKGSNL", ]

        s.board_promoted = ["         "] * 9
        s.moti = {"P": 0, "L": 0, "N": 0, "S": 0, "G": 0, "B": 0, "R": 0,
                  "p": 0, "l": 0, "n": 0, "s": 0, "g": 0, "b": 0, "r": 0, }
        return s

    @classmethod
    def shogi_pos(cls, sfen_pos):
        """sfenの位置表現を将棋の位置表現に変換する"""

        dic = {"a": 1, "b": 2, "c": 3, "d": 4, "e": 5, "f": 6, "g": 7, "h": 8, "i": 9}
        return (int(sfen_pos[0]), dic[sfen_pos[1]])

    @classmethod
    def board_pos(cls, sfen_pos):
        """sfenの位置表現を座標上の位置表現に変換する"""

        spos = cls.shogi_pos(sfen_pos)
        x = 9 - spos[0]
        y = spos[1] - 1
        return y, x

    @classmethod
    def next_state(cls, state, move, is_black):
        """盤面と手と手番より次の盤面を作成する"""

        if move[1] == "*":
            # 持ち駒
            y, x = cls.board_pos(move[2:4])
            piece = move[0]
            if is_black:
                piece = piece.upper()
            else:
                piece = piece.lower()

            # 持ち駒を減らす
            state.moti[piece] -= 1

            # 移動先に駒を配置する
            state.change_board(y, x, piece)

        else:
            # 駒を動かすケース
            from_y, from_x = cls.board_pos(move[0:2])
            to_y, to_x = cls.board_pos(move[2:4])

            # 駒を取るケースでは、持ち駒を加える
            if state.board[to_y][to_x] != " ":
                captured = state.board[to_y][to_x]
                if captured.islower():
                    state.moti[captured.upper()] += 1
                if captured.isupper():
                    state.moti[captured.lower()] += 1

            piece = state.board[from_y][from_x]
            piece_promoted = state.board_promoted[from_y][from_x]
            if len(move) == 5:
                piece_promoted = "+"

            # 移動先に駒を配置し、移動元から除去する
            state.change_board(from_y, from_x, " ")
            state.change_board_promoted(from_y, from_x, " ")
            state.change_board(to_y, to_x, piece)
            state.change_board_promoted(to_y, to_x, piece_promoted)

        return state

    @classmethod
    def moves_convert(cls, state, moves, is_black):

        ret = []
        st = state.copy()
        moves = [m.strip() for m in moves.split(" ")]

        for m in moves:
            mm = State.move_convert(st, m, is_black)
            st = State.next_state(st, m, is_black)
            is_black = not is_black
            ret.append(mm)

        return ret

    @classmethod
    def move_convert(cls, state, move, is_black):

        if move[1] == "*":
            # 持ち駒
            y, x = state.board_pos(move[2:4])
            piece = move[0]
            return cls.move_display(9 - x, y + 1, piece, " ", " ", "*", is_black)

        else:
            # 駒を動かすケース
            from_y, from_x = state.board_pos(move[0:2])
            to_y, to_x = state.board_pos(move[2:4])
            piece = state.board[from_y][from_x]
            promoted_bef = state.board_promoted[from_y][from_x]
            if len(move) == 5:
                promotion = "+"
            else:
                promotion = " "

            return cls.move_display(9 - to_x, to_y + 1, piece, promoted_bef, promotion, " ", is_black)

    @classmethod
    def move_display(cls, x, y, p, promoted_bef, promotion, uchi, is_black):
        # いくつかの表現は自然な表現ではない

        lst1 = list("１２３４５６７８９")
        lst2 = list("一二三四五六七八九")
        dic = {("p", " "): "歩",
               ("p", "+"): "と",
               ("l", " "): "香",
               ("l", "+"): "成香",
               ("n", " "): "桂",
               ("n", "+"): "成桂",
               ("s", " "): "銀",
               ("s", "+"): "成銀",
               ("g", " "): "金",
               ("b", " "): "角",
               ("b", "+"): "馬",
               ("r", " "): "飛",
               ("r", "+"): "龍",
               ("k", " "): "王", }

        ret = ""
        if is_black:
            ret += "▲"
        else:
            ret += "△"
        ret += lst1[x - 1]
        ret += lst2[y - 1]
        ret += dic[p.lower(), promoted_bef]
        if promotion == "+":
            ret += "成"
        if uchi == "*":
            ret += "（打）"

        return ret
