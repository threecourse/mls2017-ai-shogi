# -*- coding: utf-8 -*-

from bokeh.client import push_session
from bokeh.plotting import figure
from bokeh.io import curdoc
from bokeh.models import ColumnDataSource
from bokeh.layouts import column, row
from bokeh.models import Range1d, Spacer, Div
import bokeh.models.widgets as Widgets
from bokeh.models.glyphs import Text
from bokeh.models.tickers import FixedTicker

import numpy as np
from gikou import Gikou
from state import State


class EvalPlot:

    @classmethod
    def create_default(cls):
        plot = cls()
        plot._set_style(100 + 1)
        return plot

    @classmethod
    def create(cls, scores):
        plot = cls()
        last_step = len(scores) - 1
        y = np.array(scores)
        x = np.arange(0, last_step + 1)
        plot.source.data = dict(y=y, x=x)
        plot._set_style(last_step)
        return plot

    def __init__(self):
        self.plot = figure(plot_width=400, plot_height=200, toolbar_location=None, tools="")
        self.source = ColumnDataSource(data=dict(y=[], x=[]))
        self.plot.line(x="x", y="y", source=self.source)

    def _set_style(self, last_step):
        x_lim = last_step + 1
        self.plot.x_range = Range1d(0, x_lim)
        self.plot.y_range = Range1d(-2100, 2100)

        x_ticker = np.arange(0, x_lim, 20)
        y_ticker = np.arange(-2000, 2100, 500)
        self.plot.xaxis.ticker = FixedTicker(ticks=x_ticker)
        self.plot.xgrid.ticker = FixedTicker(ticks=x_ticker)
        self.plot.yaxis.ticker = FixedTicker(ticks=y_ticker)
        self.plot.ygrid.ticker = FixedTicker(ticks=y_ticker)

        self.plot.line([x_lim, x_lim], [-2100, 2100], color="black", line_width=1)
        self.plot.line([0, x_lim], [2100, 2100], color="black", line_width=1)


class BoardPlot:

    def __init__(self):

        self.board_source = ColumnDataSource(data=dict(y=[], x=[], koma=[], angle=[]))
        self.captured_source = ColumnDataSource(data=dict(y=[], x=[], koma=[], angle=[]))

        self.plot = figure(plot_width=400, plot_height=360, toolbar_location=None, tools="")
        self.plot.x_range = Range1d(-1, 10)
        self.plot.y_range = Range1d(0, 9)

        self.plot.xgrid.grid_line_color = None  # 'black'
        self.plot.ygrid.grid_line_color = None  # 'black'
        self.plot.xaxis.visible = False
        self.plot.yaxis.visible = False
        self.plot.outline_line_color = "white"

        for i in range(0, 10):
            self.plot.line([i, i], [0, 9], color="black", line_width=1)
            self.plot.line([0, 9], [i, i], color="black", line_width=1)

        glyph = Text(x="x", y="y", text="koma", angle="angle",
                     text_color="black", text_baseline="middle", text_align="center", text_font_size="20px")

        self.plot.add_glyph(self.board_source, glyph)
        self.plot.add_glyph(self.captured_source, glyph)

    def set_data(self, state):
        self.board_source.data = self._create_board_data(state)
        self.captured_source.data = self._create_captured_data(state)

    def _create_board_data(self, state):
        xs, ys, ks, angles = [], [], [], []

        for x in range(9):
            for y in range(9):
                xx = x + 0.5
                yy = 8 - y + 0.5
                koma, to_reverse = State.koma(state.board[y][x], state.board_promoted[y][x])
                if to_reverse:
                    angle = np.pi
                else:
                    angle = 0.0
                xs.append(xx)
                ys.append(yy)
                ks.append(koma)
                angles.append(angle)

        data = dict(x=xs, y=ys, koma=ks, angle=angles)
        return data

    def _create_captured_data(self, state):

        white_pieces = []
        for c in "rbgsnlp":
            ct = state.moti[c]
            for i in range(ct):
                white_pieces.append(State.koma(c, " "))

        black_pieces = []
        for c in "RGBSNLP":
            ct = state.moti[c]
            for i in range(ct):
                black_pieces.append(State.koma(c, " "))

        x, y, z, a = [], [], [], []
        x += [-1 + 0.5]
        y += [8 + 0.5]
        z += ["\u2616"]
        a += [0.0]

        x += [9 + 0.5]
        y += [8 + 0.5]
        z += ["\u2617"]
        a += [0.0]

        x += [-1 + 0.5 for i, p in enumerate(white_pieces)]
        y += [(7 + 0.5 - i * 0.8) for i, p in enumerate(white_pieces)]
        z += [p[0] for i, p in enumerate(white_pieces)]
        a += [0.0 for i, p in enumerate(white_pieces)]

        x += [9 + 0.5 for i, p in enumerate(black_pieces)]
        y += [(7 + 0.5 - i * 0.8) for i, p in enumerate(black_pieces)]
        z += [p[0] for i, p in enumerate(black_pieces)]
        a += [0.0 for i, p in enumerate(black_pieces)]

        data = dict(x=x, y=y, koma=z, angle=a)

        return data


class LayoutManager:

    def __init__(self):

        # load kifu
        with open("kifu_data_sfen.txt") as f:
            lines = f.readlines()
        lines = [line.rstrip() for line in lines]
        lines = [line[15:] for line in lines]  # remove "startpos moves"
        options = [(line, line[:50] + "...") for line in lines]  # (value, label)

        # widgets
        self.title = Div(text='棋譜解析', style={"font-family": "Meiryo", "font-size": "30px"}, width=400, height=30)
        self.input = Widgets.Select(options=options, width=300, height=50, title="SFEN形式で棋譜を入力して下さい")
        self.input.value = options[0][0]
        self.input.on_change('value', self.on_change_input)

        end_step_default = 100
        self.slider = Widgets.Slider(start=0, end=end_step_default, value=min(50, end_step_default),
                                     step=1, width=300, height=50, show_value=False, title="---")
        self.slider.on_change('value', self.on_change_slider)

        self.button1 = Widgets.Button(label="<", button_type="success", width=30, height=30)
        self.button1.on_click(self.on_click_button_prev)
        self.button2 = Widgets.Button(label=">", button_type="success", width=30, height=30)
        self.button2.on_click(self.on_click_button_next)

        self.evalplot = EvalPlot.create_default()
        self.textplot = BoardPlot()
        self.div2 = Div(text="", width=400, height=200, style={"font-family": "Meiryo"})

        self.evaluated_kifu = None
        self.result = None
        self.state = None

        # layout
        c1 = column(self.title,
                    self.input,
                    row([self.slider, Spacer(width=10), self.button1, Spacer(width=10), self.button2]),
                    self.evalplot.plot,
                    self.div2
                    )
        c2 = column(Spacer(height=150),
                    self.textplot.plot, )
        l = row([c1, c2], sizing_mode='fixed')

        self.layout_root = l

    def set_evaluation(self, kifu):
        self.evaluated_kifu = kifu
        self.result = Gikou.run(self.input.value, 100)

        evalplot = EvalPlot.create(self.result["scores"])
        self.evalplot = evalplot
        self.layout_root.children[0].children[3] = self.evalplot.plot

        last_step = len(self.result["scores"]) - 1
        self.slider.end = last_step
        self.slider.value = min(self.slider.value, self.slider.end)

    def set_board(self, steps):
        kifu = self.evaluated_kifu
        moves = kifu.split(" ")

        state = State.start_state()
        for i, m in enumerate(moves[:steps]):
            state = State.next_state(state, m, i % 2 == 0)
        self.textplot.set_data(state)
        self.state = state

    def update_info(self, steps):
        scores = self.result["scores"]
        last_step = len(scores) - 1
        _steps = min(last_step, steps)
        score = scores[_steps]
        self.slider.title = "{}手目 - 評価値: {}".format(_steps, score)

        pvs = self.result["pvs"][_steps]
        is_black = _steps % 2 == 0

        pvs_converted = State.moves_convert(self.state, pvs, is_black)
        pvs_text = ""
        for i, s in enumerate(pvs_converted):
            pvs_text += s
            if i % 4 == 3:
                pvs_text += "<br/>"
            else:
                pvs_text += " "

        self.div2.text = "読み筋:<br/>" + pvs_text

    def on_change_input(self, attr, old, new):
        kifu = self.input.value
        steps = self.slider.value
        self.set_evaluation(kifu)
        self.set_board(steps)
        self.update_info(steps)

    def on_change_slider(self, attr, old, new):
        kifu = self.input.value
        steps = self.slider.value
        if self.evaluated_kifu != kifu:
            self.set_evaluation(kifu)
        self.set_board(steps)
        self.update_info(steps)

    def on_click_button_prev(self):
        self.slider.value = max(self.slider.value - 1, 0)

    def on_click_button_next(self):
        self.slider.value = min(self.slider.value + 1, self.slider.end)


if __name__ == "__main__":
    lm = LayoutManager()
    curdoc().add_root(lm.layout_root)
    curdoc().title = "棋譜解析"

    print("bokeh plotting set")

    session = push_session(curdoc())
    session.show()
    session.loop_until_closed()
